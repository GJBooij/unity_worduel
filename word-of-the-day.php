<?php 
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class WordOfTheDay{
	public $gameId;
	public $word_nl_5;
	public $word_nl_6;
	public $serverTime;
}

$wordOfTheDay = new WordOfTheDay();

//First we figure out the game id since the app went live
date_default_timezone_set('Europe/Amsterdam');
$from = strtotime('28-01-2022 00:00:00');
$today = time();
$difference = $today - $from;
$gameId = ceil($difference / 86400);

$wordOfTheDay->gameId = $gameId;

//Include the words for this year
include 'wotd-5-2023.php';
include 'wotd-6-2023.php';

//Then figure out the word of the day
$wordOfTheDay->word_nl_5 = $words_nl_5[date('z')];
$wordOfTheDay->word_nl_6 = $words_nl_6[date('z')];
$wordOfTheDay->serverTime = $today;

echo json_encode($wordOfTheDay);

?>