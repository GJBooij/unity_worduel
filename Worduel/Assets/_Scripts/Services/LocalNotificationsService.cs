using System;
using System.Collections.Generic;
using System.Linq;
using EasyMobile;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;

namespace Worduel.Scripts.Services
{
    public class LocalNotificationsService : MonoBehaviour
    {
        //Singleton
        public static LocalNotificationsService Instance;
        
        //Inspector variables
        [SerializeField] private GameService gameService;
        
        //Private variables
        private Dictionary<int, List<string>> scheduledNotificationsWotdNL5;
        private bool configReady, gameServiceReady;
        
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {
            scheduledNotificationsWotdNL5 = LoadOrInitSavedSchedules(Constants.PlayerPrefsKeys.NOTIFCATIONS_WORDS_NL_5);

            //Subscribe to the GameState for checking when the user has either won or lost the game.
            gameService.GameStateRX.Subscribe(gameState =>
            {
                if (gameState == GameState.Won || gameState == GameState.GameOver)
                {
                    //Check if there are any notifications scheduled for this game today and remove them.
                    if (scheduledNotificationsWotdNL5.ContainsKey(ConfigDataService.Instance.WordOfTheDay.GameId))
                    {
                        foreach (string notificationId in scheduledNotificationsWotdNL5[ConfigDataService.Instance.WordOfTheDay.GameId])
                            Notifications.CancelPendingLocalNotification(notificationId);
                    }
                }
            }).AddTo(this);

            //Subscribe to the Configuration Service Status and wait for the config to be loaded.
            if (gameService.ServiceReadyRX.Value)
            {
                gameServiceReady = true;
                CreateOrAddSchedules();
            }
            else
                gameService.ServiceReadyRX.Subscribe(ready =>
                {
                    if (!ready) return;
                    gameServiceReady = true;
                    CreateOrAddSchedules();
                }).AddTo(this);
            
            //Subscribe to the Configuration Service Status and wait for the config to be loaded.
            if (ConfigDataService.Instance.ConfigServiceStatusRX.Value == ConfigServiceStatus.ConfigLoaded)
            {
                configReady = true;
                CreateOrAddSchedules();
            }
            else
                ConfigDataService.Instance.ConfigServiceStatusRX.Subscribe(status =>
                {
                    if (status == ConfigServiceStatus.ConfigLoaded)
                    {
                        configReady = true;
                        CreateOrAddSchedules();
                    }
                }).AddTo(this);
        }

        /// <summary>
        /// Function that checks the already created notification schedules and creates new ones if needed for the upcoming days/games.
        /// </summary>
        private void CreateOrAddSchedules()
        {
            if (!gameServiceReady || !configReady)
                return;
            
            //Wait for 1 second to make sure the Notifications Plugin is done initializing.
            Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(x =>
            {
                int gameIdToday = ConfigDataService.Instance.WordOfTheDay.GameId;
               
                //Get the day of the server
                DateTime day = DateTime.Now.Date;

                //Clean up all old data.
                List<int> idsToRemove = (from kvp in scheduledNotificationsWotdNL5 where kvp.Key < gameIdToday select kvp.Key).ToList();
                foreach (int i in idsToRemove)
                    scheduledNotificationsWotdNL5.Remove(i);

                //Do this for the coming 7 games
                for (int i = gameIdToday; i < gameIdToday + 7; i++)
                {
                    if (i == gameIdToday && (gameService.GameStateRX.Value == GameState.Won
                                             || gameService.GameStateRX.Value == GameState.GameOver))
                    {
                        //Check if there are any notifications scheduled for this game today and remove them as the game is already finished
                        if (scheduledNotificationsWotdNL5.ContainsKey(ConfigDataService.Instance.WordOfTheDay.GameId))
                            foreach (string notificationId in scheduledNotificationsWotdNL5[ConfigDataService.Instance.WordOfTheDay.GameId])
                                Notifications.CancelPendingLocalNotification(notificationId);
                        
                        day = day.AddDays(1);
                        continue;
                    }
                    
                    if (!scheduledNotificationsWotdNL5.ContainsKey(i))
                    {
                        List<string> notificationIds = new List<string>();
                        
                        // Prepare the notification content (see the above section).
                        DateTime triggerDate1 = new DateTime(day.Year, day.Month, day.Day, 6, 00, 00);
                        DateTime triggerDate2 = new DateTime(day.Year, day.Month, day.Day, 14, 00, 00);
                        // DateTime triggerDate3 = new DateTime(day.Year, day.Month, day.Day, 21, 00, 00);
                        string id1 = Notifications.ScheduleLocalNotification(triggerDate1, CreateWotdNotificationContent("Een nieuwe dag en een nieuw woord om te raden!"));
                        string id2 = Notifications.ScheduleLocalNotification(triggerDate2, CreateWotdNotificationContent("Vergeet niet het woord van de dag te raden :)"));
                        // string id3 = Notifications.ScheduleLocalNotification(triggerDate3, CreateWotdNotificationContent("Je hebt nog maar een paar uur om het woord van de dag te raden!"));
                        
                        notificationIds.Add(id1);
                        notificationIds.Add(id2);
                        // notificationIds.Add(id3);
                        
                        //Add the scheduled id's to the dictionary
                        scheduledNotificationsWotdNL5.Add(i, notificationIds);
                    }

                    day = day.AddDays(1);
                }
                
                //Save the scheduled notifications to the player prefs.
                SaveScheduledNotifications(Constants.PlayerPrefsKeys.NOTIFCATIONS_WORDS_NL_5, scheduledNotificationsWotdNL5);
            });
        }

        /// <summary>
        /// Function that either loads the current scheduled notification IDs from PlayerPrefs or creates a new Dictionary.
        /// </summary>
        private Dictionary<int, List<string>> LoadOrInitSavedSchedules(string playerprefkey)
        {
            Dictionary<int, List<string>> saved = new Dictionary<int, List<string>>();
            
            if (PlayerPrefs.HasKey(playerprefkey))
            {
                Dictionary<int, List<string>> saveData = JsonConvert.DeserializeObject<Dictionary<int, List<string>>>(PlayerPrefs.GetString(playerprefkey));
                if (saveData != null)
                    saved = saveData;
            }
            
            return saved;
        }

        /// <summary>
        /// Function that saves the given Dictionary of notification ID's by GameID to given PlayerPrefs key.
        /// </summary>
        private void SaveScheduledNotifications(string playerprefkey, Dictionary<int, List<string>> scheduledNotifications)
        {
            PlayerPrefs.SetString(playerprefkey, JsonConvert.SerializeObject(scheduledNotifications));
            PlayerPrefs.Save();
        }
        
        // Construct the content of a new notification for scheduling.
        private NotificationContent CreateWotdNotificationContent(string body)
        {
            NotificationContent content = new NotificationContent();
            
            // Provide the notification title.
            content.title = "Woord van de dag";
            
            // You can optionally provide the notification subtitle, which is visible on iOS only.
            // content.subtitle = "Demo Subtitle";
            
            // Provide the notification message.
            content.body = body;

            // You can optionally assign this notification to a category using the category ID.
            // If you don't specify any category, the default one will be used.
            // Note that it's recommended to use the category ID constants from the EM_NotificationsConstants class
            // if it has been generated before. In this example, UserCategory_notification_category_test is the
            // generated constant of the category ID "notification.category.test".
            content.categoryId = EM_NotificationsConstants.UserCategory_notification_wotd5_dutch;
            
            // If you want to use default small icon and large icon (on Android),
            // don't set the smallIcon and largeIcon fields of the content.
            // If you want to use custom icons instead, simply specify their names here (without file extensions).
            content.smallIcon = "ic_stat_worduel";
            content.largeIcon = "ic_large_worduel";
            return content;
        }
    }
}
