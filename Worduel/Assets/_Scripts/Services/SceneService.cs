﻿using UniRx;
using UnityEngine;
using UnityEngine.Scripting;
using UnityEngine.SceneManagement;
using Worduel.Scripts.Interfaces.Services;

namespace Worduel.Scripts.Services
{
    public enum SceneName
    {
        Start,
        DungeonScene
    }
    
    [Preserve]
    public class SceneService : ISceneService
    {
        public IReactiveProperty<SceneName> CurrentSceneRX { get; }

        private AsyncOperation loadSceneOperation;
        private SceneName sceneToLoad;
        private SceneName previousSceneName;

        public SceneService()
        {
            Application.targetFrameRate = 30;
            CurrentSceneRX = new ReactiveProperty<SceneName>();
            previousSceneName = SceneName.Start;
        }

        public void GoToScene(SceneName scene)
        {
            sceneToLoad = scene;
            loadSceneOperation = SceneManager.LoadSceneAsync(sceneToLoad.ToString());
            loadSceneOperation.completed += OnSceneLoadCompleted;
        }
        
        public void GoToPreviousScene()
        {
            sceneToLoad = previousSceneName;
            loadSceneOperation = SceneManager.LoadSceneAsync(sceneToLoad.ToString());
            loadSceneOperation.completed += OnSceneLoadCompleted;
        }

        private void OnSceneLoadCompleted(AsyncOperation operation)
        {
            loadSceneOperation.completed -= OnSceneLoadCompleted;
            previousSceneName = CurrentSceneRX.Value;
            CurrentSceneRX.Value = sceneToLoad;
        }
    }
}
