﻿using System;
using UniRx;
using UnityEngine;
using Worduel.Scripts.Interfaces.Services;

namespace Worduel.Scripts.Services
{
    public class DeeplinkService : IDeeplinkService
    {
        public Subject<string> DeeplinkRX = new Subject<string>();

        public DeeplinkService()
        {
            Application.deepLinkActivated += onDeepLinkActivated;
            if (!String.IsNullOrEmpty(Application.absoluteURL))
            {
                // Cold start and Application.absoluteURL not null so process Deep Link.
                onDeepLinkActivated(Application.absoluteURL);
            }
        }
        
        private void onDeepLinkActivated(string url)
        {
            Debug.Log("[DeeplinkService] - new deeplink: " + url);
            
            // Decode the URL to determine action. 
            // In this example, the app expects a link formatted like this:
            // identifier://link?paymentsuccess
            // identifier://link?paymentfailed
            string deeplink = url.Split("?"[0])[1];
            
            // Update DeepLink variable
            DeeplinkRX.OnNext(deeplink);
        }
    }
}