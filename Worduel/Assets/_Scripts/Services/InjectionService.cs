﻿using Adic.Container;
using UnityEngine.Scripting;
using Worduel.Scripts.Interfaces.Services;

namespace Worduel.Scripts.Services
{
    [Preserve]
    public class InjectionService : IInjectionService
    {
        public InjectionService(IInjectionContainer container)
        {
            Container = container;
        }

        public IInjectionContainer Container { get; }
    }
}
