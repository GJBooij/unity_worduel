using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Adic;
using NetCheckerLib.Net;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;
using worduel.models;
using Worduel.Scripts.UI;

namespace Worduel.Scripts.Services
{
    public enum ConfigServiceStatus
    {
        None,
        CheckingInternet,
        NoInternet,
        GettingConfig,
        ForceUpdate,
        AppDisabled,
        ConfigLoaded
    }
    
    public class ConfigDataService : MonoBehaviour
    {
        //Singleton
        public static ConfigDataService Instance;

        //Reactive variables
        public IReactiveProperty<ConfigServiceStatus> ConfigServiceStatusRX = new ReactiveProperty<ConfigServiceStatus>();

        //Public variables
        [HideInInspector] public AppConfiguration AppConfig;
        [HideInInspector] public WordOfTheDay WordOfTheDay;
        [HideInInspector] public WordsDatabase Words_nl_5;
        [HideInInspector] public WordsDatabase Words_nl_6;
        
        //Injections
        [Inject] protected UIService uiService;
        
        //Private variables
        private DateTime appOpenDate;
        private bool appLostFocus;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);

            // PlayerPrefs.DeleteAll();
            
#if UNITY_ANDROID
            Screen.fullScreen = false;
#endif
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Application.targetFrameRate = 60;
            appOpenDate = DateTime.Now.Date;
        }

        void Start()
        {
            DoPPKMigration();
            
            //Wait for the next frame to make sure the UI service is also setup correctly.
            Observable.NextFrame().Subscribe(x =>
            {
                NetChecker.ShowDebug = false;
                NetChecker.Init();
                CheckInternetAndGetConfigs();
            });
            
            this.Inject();
        }

        public void CheckInternetAndGetConfigs()
        {
            ConfigServiceStatusRX.Value = ConfigServiceStatus.CheckingInternet;
            
            //Register to the internet checker
            NetChecker.OnCheckFinished += InternetCheckFinished;
            NetChecker.OnCheckTimeout += InternetCheckFinished;
            NetChecker.CheckConnection();

            // Internet Check Callback
            void InternetCheckFinished()
            {
                //No longer listen to network check
                NetChecker.OnCheckFinished -= InternetCheckFinished;
                NetChecker.OnCheckTimeout -= InternetCheckFinished;
                
                //Check the network status, are we connected to the internet?
                if (NetChecker.Status != NetStatus.Connected)
                {
                    //If not, set the status to no internet. 
                    ConfigServiceStatusRX.Value = ConfigServiceStatus.NoInternet;
                }
                else
                {
                    //If yes, get the remote configurations.
                    StartCoroutine(GetConfigs());
                }
            }
        }

        // Start is called before the first frame update
        IEnumerator GetConfigs()
        {
            ConfigServiceStatusRX.Value = ConfigServiceStatus.GettingConfig;
            
            yield return new WaitForEndOfFrame();

            //Fetch the app configuration
            using (UnityWebRequest webRequest = UnityWebRequest.Get(Constants.Endpoints.APP_CONFIG))
            {
                webRequest.certificateHandler = new CertificateWhore();
                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();
                if (webRequest.result == UnityWebRequest.Result.Success)
                {
                    AppConfig = JsonConvert.DeserializeObject<AppConfiguration>(webRequest.downloadHandler.text);
                    if (AppConfig == null || AppConfig.AppDisabled)
                    {
                        ConfigServiceStatusRX.Value = ConfigServiceStatus.AppDisabled;
                        yield break;
                    }
                }
                else
                {
                    ConfigServiceStatusRX.Value = ConfigServiceStatus.AppDisabled;
                    yield break;
                }
            }
            
            //Check for the Force update
#if UNITY_ANDROID && !UNITY_EDITOR
            if (int.Parse(AndroidBridge.GetBuildVersion()) < AppConfig.MinimumBuildNumberAndroid)
            {
                ConfigServiceStatusRX.Value = ConfigServiceStatus.ForceUpdate;
                yield break;
            }
#elif UNITY_IOS && !UNITY_EDITOR
            if (iOSBuildVersionChecker.GetBuildVersion() < AppConfig.MinimumBuildNumberIOS)
            {
                ConfigServiceStatusRX.Value = ConfigServiceStatus.ForceUpdate;
                yield break;
            }
#elif UNITY_EDITOR
            // ConfigServiceStatusRX.Value = ConfigServiceStatus.ForceUpdate;
            // yield break;
#endif

            //Load the cached words database
            Words_nl_5 = LoadWordsDatabase(Constants.PlayerPrefsKeys.WORDS_NL_5);
            Words_nl_6 = LoadWordsDatabase(Constants.PlayerPrefsKeys.WORDS_NL_6);
            
            //Check word databases for versions
            if (Words_nl_5 == null || Words_nl_5.Version < AppConfig.DBVersionNL5)
            {
                //Fetch the dutch words database for 5 letters
                using (UnityWebRequest webRequest = UnityWebRequest.Get(Constants.Endpoints.WORDSDB_NL_5))
                {
                    webRequest.certificateHandler = new CertificateWhore();
                    // Request and wait for the desired page.
                    yield return webRequest.SendWebRequest();
                    if (webRequest.result == UnityWebRequest.Result.Success)
                    {
                        Words_nl_5 = JsonConvert.DeserializeObject<WordsDatabase>(webRequest.downloadHandler.text);
                        SaveWordsDatabase(Words_nl_5, Constants.PlayerPrefsKeys.WORDS_NL_5);
                    }
                }
            }
            
            if (Words_nl_6 == null || Words_nl_6.Version < AppConfig.DBVersionNL5)
            {
                //Fetch the dutch words database for 6 letters
                using (UnityWebRequest webRequest = UnityWebRequest.Get(Constants.Endpoints.WORDSDB_NL_6))
                {
                    webRequest.certificateHandler = new CertificateWhore();
                    // Request and wait for the desired page.
                    yield return webRequest.SendWebRequest();
                    if (webRequest.result == UnityWebRequest.Result.Success)
                    {
                        Words_nl_6 = JsonConvert.DeserializeObject<WordsDatabase>(webRequest.downloadHandler.text);
                        SaveWordsDatabase(Words_nl_6, Constants.PlayerPrefsKeys.WORDS_NL_6);
                    }
                }
            }
            
            //DEBUG - New word list for year
            // List<string> words5 = new List<string>(Words_nl_5.Words);
            // List<string> words6 = new List<string>(Words_nl_6.Words);
            // words5.Shuffle();
            // words6.Shuffle();
            // Debug.Log(JsonConvert.SerializeObject(words5.Take(370)));
            // Debug.Log(JsonConvert.SerializeObject(words6.Take(370)));
            
            //Fetch the Word Of The Day data
            if (Constants.LOCAL_WOTD)
            {
                TextAsset targetFile = Resources.Load<TextAsset>("data/word-of-the-day");
                WordOfTheDay = JsonConvert.DeserializeObject<WordOfTheDay>(targetFile.text);
                Debug.LogWarning("Word of the Day - Loaded Locally! Game id: " + WordOfTheDay.GameId);
            }
            else
            {
                using (UnityWebRequest webRequest = UnityWebRequest.Get(Constants.Endpoints.WORD_OF_THE_DAY))
                {
                    webRequest.certificateHandler = new CertificateWhore();
                    // Request and wait for the desired page.
                    yield return webRequest.SendWebRequest();
                    if (webRequest.result == UnityWebRequest.Result.Success)
                    {
                        WordOfTheDay = JsonConvert.DeserializeObject<WordOfTheDay>(webRequest.downloadHandler.text);
                    }
                }
            }
            
            ConfigServiceStatusRX.Value = ConfigServiceStatus.ConfigLoaded;
        }

        private WordsDatabase LoadWordsDatabase(string playerprefkey)
        {
            if (PlayerPrefs.HasKey(playerprefkey))
                return JsonConvert.DeserializeObject<WordsDatabase>(PlayerPrefs.GetString(playerprefkey));

            return null;
        }

        private void SaveWordsDatabase(WordsDatabase database, string playerprefkey)
        {
            string saveData = JsonConvert.SerializeObject(database);
            PlayerPrefs.SetString(playerprefkey, saveData);
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Function called by the system when the application gains or loses focus.
        /// </summary>
        private void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                appLostFocus = true;
            }
            else if(ConfigServiceStatusRX.Value == ConfigServiceStatus.ConfigLoaded)
            {
                DateTime dateNow = DateTime.Now.Date;
                if (appOpenDate < dateNow)
                {
                    appOpenDate = dateNow;
                    uiService.GoToView(ViewType.Home);
                    CheckInternetAndGetConfigs();
                }
                
                appLostFocus = false;
            }
        }

        /// <summary>
        /// Function to perform any Player Prefs Migrations for the current version of the app.
        /// </summary>
        private void DoPPKMigration()
        {
            foreach (KeyValuePair<string, string> kvp in Constants.PlayerPrefMigrations.Build5to6)
            {
                if (PlayerPrefs.HasKey(kvp.Key) && !PlayerPrefs.HasKey(kvp.Value))
                {
                    string savedString = PlayerPrefs.GetString(kvp.Key);
                    PlayerPrefs.DeleteKey(kvp.Key);
                    PlayerPrefs.SetString(kvp.Value, savedString);
                }
            }
            
            PlayerPrefs.Save();
        }
    }
    
    public class CertificateWhore : CertificateHandler
    {
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            return true;
        }
    }
}
