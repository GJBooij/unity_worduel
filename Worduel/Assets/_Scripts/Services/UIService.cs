﻿using System;
using UniRx;
using UnityEngine;
using System.Collections.Generic;
using Worduel.Scripts.UI;

namespace Worduel.Scripts.Services
{
    public enum UIServiceState
    {
        View,
        Modal
    }
    
    public enum UIState
    {
        Inactive,
        Active
    }
    
    public class UIService : MonoBehaviour
    {
        //Public (reactive) variables
        public IReactiveProperty<UIServiceState> StateRX { get; private set; }
        public IReactiveProperty<ViewType> CurrentViewRX = new ReactiveProperty<ViewType>();
        public ModalType CurrentModal { get; private set; }
        
        //Private variables
        private IDictionary<ModalType, BaseModal> modals;
        private IDictionary<ViewType, BaseView> views;
        private TopBar topBar;

        private void Awake()
        {
            topBar = transform.Find("TopBar").GetComponent<TopBar>();
            
            SetupViews();
            SetupModals();
            
            StateRX = new ReactiveProperty<UIServiceState>(UIServiceState.View);
        }

        private void Start()
        {
            topBar.Hide();
            CurrentViewRX.Value = ViewType.Home;
            views[ViewType.Home].Show(CurrentViewRX.Value, true);
        }

        public void GoToView(ViewType viewTypeId,  bool shouldRefresh = false, params object[] viewParams)
        {
            BaseView currentView = views[CurrentViewRX.Value];
            BaseView newView = views[viewTypeId];

            currentView.Hide();
            newView.Show(currentView.Id, shouldRefresh, viewParams);

            newView.StateRX
                .Where(x => x == UIState.Active)
                .First()
                .Subscribe(x =>
                {
                    CurrentViewRX.Value = viewTypeId;
                    StateRX.Value = UIServiceState.View;
                })
                .AddTo(this);
        }
        
        public void ShowModal(ModalType modalTypeId, params object[] modalParams)
        {
            modals[modalTypeId].Show(modalParams);

            CurrentModal = modalTypeId;
            
            StateRX.Value = UIServiceState.Modal;
        }

        public void HideModal()
        {
            if (CurrentModal == ModalType.None)
                return;

            modals[CurrentModal].Hide();
            StateRX.Value = UIServiceState.View;
        }
        
        public void ShowTopBar(){topBar.Show();}
        public void HideTopBar(){topBar.Hide();}
        
        private void SetupViews()
        {
            views = new Dictionary<ViewType, BaseView>();
            Transform viewsWrapper = transform.Find("Views");

            foreach (Transform view in viewsWrapper)
            {
                BaseView baseView = view.GetComponent<BaseView>();
                views.Add(baseView.Id, baseView);
                
                if(baseView.Id != ViewType.Home)
                    baseView.Hide();
            }
        }
        
        private void SetupModals()
        {
            modals = new Dictionary<ModalType, BaseModal>();
            Transform modalsWrapper = transform.Find("Modals");

            foreach (Transform modal in modalsWrapper)
            {
                BaseModal baseModal = modal.GetComponent<BaseModal>();
                modals.Add(baseModal.Id, baseModal);
            }
        }
        
#if UNITY_ANDROID
//////////        
// PURELY FOR ANDROID BACK BUTTON TO QUIT //
//////////
        private bool backPressed;
        private void SetBackPressedFalse() { backPressed = false; }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!backPressed)
                {
                    backPressed = true;
                    Utils.ShowAndroidToastMessage("Druk nogmaals om Woorduel te sluiten");
                    Invoke("SetBackPressedFalse", 3.0f);
                }
                else
                {
                    Application.Quit();
                }
            }
        }
#endif
    }
}