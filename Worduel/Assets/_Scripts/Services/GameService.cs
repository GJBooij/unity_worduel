using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Adic;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;
using worduel.models;
using Worduel.Scripts.UI;

public enum GameState
{
    Playing,
    Checking,
    GameOver,
    Won
}

public enum GameType
{
    WOTD_5,
    WOTD_6
}

namespace Worduel.Scripts.Services
{
    public class GameService : MonoBehaviour
    {
        //Public variables
        public IReactiveProperty<bool> ServiceReadyRX;
        public IReactiveProperty<GameState> GameStateRX;
        public ISubject<Dictionary<string, LetterStatus>> LetterStatusesUpdatedRX;
        public ISubject<string> UnknownWordRX;
        
        //Inspector variables
        [SerializeField] private GameType gameType;
        [SerializeField] private LetterboxGrid lettersGrid;

        //Injections
        [Inject] protected UIService uiService;
        
        //Private variables
        private Dictionary<string, LetterStatus> lettersAndStatuses = new Dictionary<string, LetterStatus>();
        private WOTDGlobalSaveData globalSaveData;
        private string PPK_WOTD, PPKWOTD_GLOBAL;

        private void Awake()
        {
            ServiceReadyRX = new ReactiveProperty<bool>();
            GameStateRX = new ReactiveProperty<GameState>();
            LetterStatusesUpdatedRX = new Subject<Dictionary<string, LetterStatus>>();
            UnknownWordRX = new Subject<string>();

            PPK_WOTD = gameType == GameType.WOTD_5 ? Constants.PlayerPrefsKeys.SAVEDATA_WOTD_NL_5 : Constants.PlayerPrefsKeys.SAVEDATA_WOTD_NL_6;
            PPKWOTD_GLOBAL = gameType == GameType.WOTD_5 ? Constants.PlayerPrefsKeys.SAVEDATA_GLOBAL_WOTD_NL_5 : Constants.PlayerPrefsKeys.SAVEDATA_GLOBAL_WOTD_NL_6;
        }

        void Start()
        {
            ConfigDataService.Instance.ConfigServiceStatusRX.Subscribe(status =>
            {
                if (status == ConfigServiceStatus.ConfigLoaded)
                {
                    //Load in save data
                    globalSaveData = LoadWOTDGlobalSaveData(PPKWOTD_GLOBAL);
                    WOTDSaveData saveData = LoadWOTDSaveData(PPK_WOTD);
                    
                    //Check current game savedata.
                    if (saveData != null && saveData.GameId == ConfigDataService.Instance.WordOfTheDay.GameId)
                    {
                        lettersGrid.Init(saveData.RowSpelledWords, saveData.RowLetterStatuses);
                        lettersAndStatuses = saveData.GlobalLetterStatuses;

                        //Check the final entry to get the correct game state
                        CheckWord(true);
                    }
                    else
                    {
                        lettersGrid.Init();
                        lettersAndStatuses.Clear();
                        LetterStatusesUpdatedRX.OnNext(lettersAndStatuses);
                        GameStateRX.Value = GameState.Playing;
                    }

                    ServiceReadyRX.Value = true;
                }
                else
                    ServiceReadyRX.Value = false;
            });
            
            this.Inject();
        }

        public void CheckWord(bool fromSave = false)
        {
            GameStateRX.Value = GameState.Checking;

            //Get the word of the day and the word that the player spelled.
            string word = gameType == GameType.WOTD_5 ? ConfigDataService.Instance.WordOfTheDay.WordNL5.ToUpper() : ConfigDataService.Instance.WordOfTheDay.WordNL6.ToUpper();
            string playerWord = lettersGrid.GetCurrentRow().GetSpelledWord().ToUpper();

            if (gameType == GameType.WOTD_5 
                && !ConfigDataService.Instance.Words_nl_5.Words.Contains(playerWord, StringComparer.OrdinalIgnoreCase))
            {
                UnknownWordRX.OnNext(playerWord.ToUpper());
                GameStateRX.Value = GameState.Playing;
                return;
            }
            if (gameType == GameType.WOTD_6 
                && !ConfigDataService.Instance.Words_nl_6.Words.Contains(playerWord, StringComparer.OrdinalIgnoreCase))
            {
                UnknownWordRX.OnNext(playerWord.ToUpper());
                GameStateRX.Value = GameState.Playing;
                return;
            }

            List<LetterStatus> statuses = new List<LetterStatus>();
            List<int> goodGuessIndices = new List<int>();
            Dictionary<string, int> almostCounterByLetter = new Dictionary<string, int>();

            //First check which letters are correct
            for (int i = 0; i < playerWord.Length; i++)
            {
                LetterStatus status = LetterStatus.None;
                if (playerWord[i].ToString().Equals(word[i].ToString()))
                {
                    status = LetterStatus.Good;
                    goodGuessIndices.Add(i);
                    
                    if(almostCounterByLetter.ContainsKey(playerWord[i].ToString()))
                        almostCounterByLetter[playerWord[i].ToString()]++;
                    else
                        almostCounterByLetter.Add(playerWord[i].ToString(), 1);
                    
                    if (lettersAndStatuses.ContainsKey(playerWord[i].ToString()))
                        lettersAndStatuses[playerWord[i].ToString()] = status;
                    else
                        lettersAndStatuses.Add(playerWord[i].ToString(), status);
                }
                
                statuses.Add(status);
            }

            //Check each letter against the letter of the actual word.
            for (int i = 0; i < playerWord.Length; i++)
            {
                if(statuses[i] == LetterStatus.Good) continue;

                bool updateGlobalStatusses = true;
                LetterStatus status = LetterStatus.Wrong;
                if (word.Contains(playerWord[i].ToString()))
                {
                    if (!lettersAndStatuses.ContainsKey(playerWord[i].ToString()))
                    {
                        status = LetterStatus.Almost;
                        if(almostCounterByLetter.ContainsKey(playerWord[i].ToString()))
                            almostCounterByLetter[playerWord[i].ToString()]++;
                        else
                            almostCounterByLetter.Add(playerWord[i].ToString(), 1);
                    }
                    else
                    {
                        LetterStatus currentLetterStatus = lettersAndStatuses[playerWord[i].ToString()];
                        if (currentLetterStatus == LetterStatus.Good || currentLetterStatus == LetterStatus.Almost)
                        {
                            if (playerWord.ToCharArray().Count(c => c == playerWord[i]) > 1)
                            {
                                if (word.ToCharArray().Count(c => c == playerWord[i]) > 1)
                                {
                                    if (almostCounterByLetter.ContainsKey(playerWord[i].ToString())
                                        && almostCounterByLetter[playerWord[i].ToString()] >=
                                        word.ToCharArray().Count(c => c == playerWord[i]))
                                    {
                                        status = LetterStatus.Wrong;
                                        updateGlobalStatusses = false;
                                    }
                                    else
                                    {
                                        status = LetterStatus.Almost;
                                        if(almostCounterByLetter.ContainsKey(playerWord[i].ToString()))
                                            almostCounterByLetter[playerWord[i].ToString()]++;
                                        else
                                            almostCounterByLetter.Add(playerWord[i].ToString(), 1);
                                    }
                                }
                                else
                                {
                                    if(almostCounterByLetter.ContainsKey(playerWord[i].ToString()))
                                        status = LetterStatus.Wrong;
                                    else
                                    {
                                        status = LetterStatus.Almost;
                                        almostCounterByLetter.Add(playerWord[i].ToString(), 1);
                                    }
                                    
                                    updateGlobalStatusses = false;
                                }
                            }
                            else
                                status = LetterStatus.Almost;
                        }
                        else
                            updateGlobalStatusses = false;
                    }
                }

                statuses[i] = status;

                if (!updateGlobalStatusses) continue;
                if (lettersAndStatuses.ContainsKey(playerWord[i].ToString()))
                    lettersAndStatuses[playerWord[i].ToString()] = status;
                else
                    lettersAndStatuses.Add(playerWord[i].ToString(), status);

            }

            //Send the letter statuses to the current row.
            lettersGrid.GetCurrentRow().DeselectAll();
            lettersGrid.GetCurrentRow().SetLetterStatuses(statuses, !fromSave);

            float waitTimer = fromSave ? 0.01f : gameType == GameType.WOTD_5 ? (5 * 0.4f) : (6 * 0.4f);
            Observable.Timer(TimeSpan.FromSeconds(waitTimer)).Subscribe(x =>
            {
                //Send update on letter statuses
                LetterStatusesUpdatedRX.OnNext(lettersAndStatuses);

                //Check if the player has won, lost or can continue.
                if (playerWord.ToUpper().Equals(word.ToUpper()))
                {
                    GameStateRX.Value = GameState.Won;

                    if (!fromSave)
                    {
                        UpdateAndSaveGlobalSaveData(PPKWOTD_GLOBAL);
                        uiService.ShowModal(ModalType.PlayerStats, GetGlobalSaveData(), lettersGrid.GetAllRowsStatuses(), gameType);
                    }
                }
                else
                {
                    if (!lettersGrid.GoToNextRow())
                    {
                        GameStateRX.Value = GameState.GameOver;

                        if (!fromSave)
                        {
                            UpdateAndSaveGlobalSaveData(PPKWOTD_GLOBAL);
                            uiService.ShowModal(ModalType.PlayerStats, GetGlobalSaveData(), lettersGrid.GetAllRowsStatuses(), gameType);
                        }
                    }
                    else
                        GameStateRX.Value = GameState.Playing;
                }

                //Create a WOTD save data
                WOTDSaveData newSaveData = new WOTDSaveData();
                newSaveData.GameId = ConfigDataService.Instance.WordOfTheDay.GameId;
                newSaveData.RowSpelledWords = lettersGrid.GetAllRowsSpelledWords();
                newSaveData.RowLetterStatuses = lettersGrid.GetAllRowsStatuses();
                newSaveData.GlobalLetterStatuses = lettersAndStatuses;
                
                //Save the WOTD save data
                SaveWotdGameProgress(PPK_WOTD, newSaveData);
            });
        }

        public WOTDGlobalSaveData GetGlobalSaveData()
        {
            return globalSaveData;
        }

        public GameType GetGameType()
        {
            return gameType;
        }

        /// <summary>
        /// Function to save the current game's progress.
        /// </summary>
        private void SaveWotdGameProgress(string playerprefkey, WOTDSaveData newSaveData)
        {
            PlayerPrefs.SetString(playerprefkey, JsonConvert.SerializeObject(newSaveData));
            PlayerPrefs.Save();
        }

        private WOTDSaveData LoadWOTDSaveData(string playerprefkey)
        {
            if (PlayerPrefs.HasKey(playerprefkey))
            {
                WOTDSaveData saveData = JsonConvert.DeserializeObject<WOTDSaveData>(PlayerPrefs.GetString(playerprefkey));
                if (saveData != null && saveData.GameId > 0)
                    return saveData;
                else
                    return null;
            }

            return null;
        }

        /// <summary>
        /// Function to save the global finished games data.
        /// </summary>
        /// <param name="playerprefkey"></param>
        private void UpdateAndSaveGlobalSaveData(string playerprefkey)
        {
            if (GameStateRX.Value == GameState.Playing || GameStateRX.Value == GameState.Checking)
                return;
            
            int gameId = ConfigDataService.Instance.WordOfTheDay.GameId;
            
            WOTDFinishedGameData newFinishedData = new WOTDFinishedGameData();
            newFinishedData.GameId = gameId;
            newFinishedData.Guesses = lettersGrid.GetRowIndex() + 1;
            newFinishedData.Letters = gameType == GameType.WOTD_5 ? 5 : 6;
            newFinishedData.Word = gameType == GameType.WOTD_5 ? ConfigDataService.Instance.WordOfTheDay.WordNL5 : ConfigDataService.Instance.WordOfTheDay.WordNL6;
            newFinishedData.MaxGuesses = 6;

            if (GameStateRX.Value == GameState.Won)
            {
                if(!globalSaveData.FinishedGames.ContainsKey(gameId))
                    globalSaveData.CurrentStreak++;
                
                if (globalSaveData.HighestStreak < globalSaveData.CurrentStreak)
                    globalSaveData.HighestStreak = globalSaveData.CurrentStreak;
                
                if(!globalSaveData.GamesWon.Contains(gameId))
                    globalSaveData.GamesWon.Add(gameId);
                
            }else if (GameStateRX.Value == GameState.GameOver)
            {
                if(!globalSaveData.FinishedGames.ContainsKey(gameId))
                    globalSaveData.CurrentStreak = 0;
                
                if(!globalSaveData.GamesLost.Contains(gameId))
                    globalSaveData.GamesLost.Add(gameId);
            }
            
            if(globalSaveData.FinishedGames.ContainsKey(gameId))
                globalSaveData.FinishedGames[gameId] = newFinishedData;
            else
                globalSaveData.FinishedGames.Add(gameId, newFinishedData);
            
            PlayerPrefs.SetString(playerprefkey, JsonConvert.SerializeObject(globalSaveData));
            PlayerPrefs.Save();
        }
        
        private WOTDGlobalSaveData LoadWOTDGlobalSaveData(string playerprefkey)
        {
            if (PlayerPrefs.HasKey(playerprefkey))
            {
                WOTDGlobalSaveData saveData = JsonConvert.DeserializeObject<WOTDGlobalSaveData>(PlayerPrefs.GetString(playerprefkey));
                return saveData ?? _CreatNew();
            }
            
            return _CreatNew();
            
            WOTDGlobalSaveData _CreatNew()
            {
                WOTDGlobalSaveData newData = new WOTDGlobalSaveData();
                newData.FinishedGames = new Dictionary<int, WOTDFinishedGameData>();
                newData.GamesWon = new List<int>();
                newData.GamesLost = new List<int>();
                return newData;
            }
        }
    }
}
