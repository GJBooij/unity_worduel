using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public static bool LOCAL_WOTD = false;
    
    public static class Endpoints
    {
        public static string APP_CONFIG = "https://zephirostudios.com/data/worduel/app_config.json";
        public static string WORDSDB_NL_5 = "https://zephirostudios.com/data/worduel/words_nl_5.json";
        public static string WORDSDB_NL_6 = "https://zephirostudios.com/data/worduel/words_nl_6.json";
        public static string WORD_OF_THE_DAY = "https://zephirostudios.com/data/worduel/word-of-the-day.php";
    }
    
    public static class Colors
    {
        public static Color WrongColor = new Color(0.470f, 0.470f, 0.470f, 1.0f);
        public static Color CorrectColor = new Color(0.435f, 0.815f, 0.549f, 1.0f);
        public static Color AlmostColor = new Color(0.962f, 0.895f, 0.385f, 1.0f);
        public static Color NormalColor = new Color(0.658f, 0.878f, 1.0f, 1.0f);
        public static Color NormalKeyColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        public static Color DisabledColor = new Color(0.754f, 0.754f, 0.754f, 1.0f);
    }

    public static class PlayerPrefsKeys
    {
        public static string ONBOARDING_GAME = "ONBOARDING_GAME";
        public static string SAVEDATA_WOTD_NL_5 = "SAVEDATA_WOTD_NL_5";
        public static string SAVEDATA_WOTD_NL_6 = "SAVEDATA_WOTD_NL_6";
        public static string SAVEDATA_GLOBAL_WOTD_NL_5 = "SAVEDATA_GLOBAL_WOTD_NL_5";
        public static string SAVEDATA_GLOBAL_WOTD_NL_6 = "SAVEDATA_GLOBAL_WOTD_NL_6";
        public static string WORDS_NL_5 = "DB_WORDS_NL_5";
        public static string WORDS_NL_6 = "DB_WORDS_NL_6";
        public static string NOTIFCATIONS_WORDS_NL_5 = "NOTIFICATIONS_WORDS_NL_5";
        public static string NOTIFCATIONS_WORDS_NL_6 = "NOTIFICATIONS_WORDS_NL_6";
        
        //Migrated
        public static string SAVEDATA_WOTD_NL = "SAVEDATA_WOTD_NL";
        public static string SAVEDATA_GLOBAL_WOTD_NL = "SAVEDATA_GLOBAL_WOTD_NL";
    }

    public static class PlayerPrefMigrations
    {
        public static Dictionary<string, string> Build5to6 = new Dictionary<string, string>()
        {
            {PlayerPrefsKeys.SAVEDATA_WOTD_NL, PlayerPrefsKeys.SAVEDATA_WOTD_NL_5},
            {PlayerPrefsKeys.SAVEDATA_GLOBAL_WOTD_NL, PlayerPrefsKeys.SAVEDATA_GLOBAL_WOTD_NL_5}
        };
    }
}
