﻿using Adic;
using System;
using System.Linq;
using Adic.Binding;
using Adic.Container;
using Adic.Injection;
using UnityEngine.Scripting;
using System.Collections.Generic;
using Worduel.Scripts.Interfaces.Services;
using Worduel.Scripts.Services;

namespace Worduel.Scripts.DependencyInjectionContainers
{
    [Preserve]
    public class SharedContainerExtension : IContainerExtension
    {
        private static IInjectionContainer sharedContainer;

        public void Init(IInjectionContainer container)
        {
        }

        public void OnRegister(IInjectionContainer container)
        {
            container.beforeResolve += BeforeResolve;
        }

        public void OnUnregister(IInjectionContainer container)
        {
            container.beforeResolve -= BeforeResolve;
        }

        private static IInjectionContainer CreateSharedContainer()
        {
            var container = new InjectionContainer()
                .RegisterExtension<EventCallerContainerExtension>()
                .RegisterExtension<UnityBindingContainerExtension>();

            container
                .Bind<IDeeplinkService>().ToSingleton<DeeplinkService>()
                .Bind<ISceneService>().ToSingleton<SceneService>();

            return container;
        }

        private bool BeforeResolve(IInjector source, Type type, InjectionMember member, object parentInstance, object identifier, ref object resolutionInstance)
        {
            if (sharedContainer == null)
                sharedContainer = CreateSharedContainer();

            IList<BindingInfo> bindings;

            if (type.IsInterface)
                bindings = sharedContainer.GetBindingsFor(type);
            else
                bindings = sharedContainer.GetBindingsTo(type);

            if (bindings == null || !bindings.Any())
                return true;

            resolutionInstance = sharedContainer.Resolve(type);

            return false;
        }
    }
}
