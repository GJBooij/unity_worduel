﻿using Adic;
using Adic.Container;
using UnityEngine;
using Worduel.Scripts.Interfaces.Services;
using Worduel.Scripts.Services;

namespace Worduel.Scripts.DependencyInjectionContainers
{
    public class MainContainer : ContextRoot
    {
        public override void SetupContainers()
        {
            IInjectionContainer container = new InjectionContainer()
                .RegisterExtension<SharedContainerExtension>();

            IInjectionService injectionService = new InjectionService(container);
            
            container
                .Bind<UIService>().ToGameObject(GameObject.Find("UIService"))
                .Bind<IInjectionService>().To(injectionService);
            
            AddContainer(container);
        }

        public override void Init()
        {
        }
    }
}
