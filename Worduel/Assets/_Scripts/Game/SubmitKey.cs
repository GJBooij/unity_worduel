using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class SubmitKey : MonoBehaviour
{
    [SerializeField] private Keyboard keyboard;
    [SerializeField] private Button keyButton;
    [SerializeField] private Image background;

    private void Start()
    {
        keyButton.interactable = false;
        background.color = Constants.Colors.NormalKeyColor;
        keyboard.CanSubmitRX.Subscribe(canSubmit => { keyButton.interactable = canSubmit; }).AddTo(this);
        keyButton.OnClickAsObservable().Subscribe(x => { keyboard.CheckWordSubRX.OnNext(true); }).AddTo(this);
    }
}
