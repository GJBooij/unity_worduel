using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Keyboard : MonoBehaviour
{
    public ISubject<string> LetterSubRX = new Subject<string>();
    public ISubject<bool> RemoveSubRX = new Subject<bool>();
    public ISubject<bool> CheckWordSubRX = new Subject<bool>();
    public ISubject<bool> CanSubmitRX = new Subject<bool>();
    public ISubject<Dictionary<string, LetterStatus>> LetterStatusesUpdatedRX = new Subject<Dictionary<string, LetterStatus>>();
}
