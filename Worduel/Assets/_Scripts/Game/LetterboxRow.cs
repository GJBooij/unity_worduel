using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

public class LetterboxRow : MonoBehaviour
{
    public IReactiveProperty<bool> IsRowFilledRX = new ReactiveProperty<bool>();
    
    public bool isFirstRow;
    private List<Letterbox> letterBoxes;
    private Letterbox currentLetterBox;
    private CompositeDisposable disposables;

    public void Init(string savedSpelledWord = "", List<LetterStatus> savedLetterStatuses = null)
    {
        disposables?.Dispose();
        disposables = new CompositeDisposable();
        letterBoxes = new List<Letterbox>();

        int letterIndex = 0;
        foreach (Transform child in transform)
        {
            Letterbox letterbox = child.GetComponent<Letterbox>();
            
            LetterStatus savedLetterStatus = LetterStatus.None;
            if (savedLetterStatuses != null)
                savedLetterStatus = savedLetterStatuses[letterIndex];
            
            letterbox.Init(string.IsNullOrEmpty(savedSpelledWord) ? "" : savedSpelledWord[letterIndex].ToString(), savedLetterStatus);
            letterBoxes.Add(letterbox);
            letterIndex++;
        }

        DeselectAll();
        Subscribe();
        IsRowFilledRX.Value = false;
    }

    public void DeselectAll()
    {
        foreach (Letterbox letterBox in letterBoxes)
            letterBox.DeselectBox();
    }
    
    public void DeselectAllBut(Letterbox exceptLetterbox)
    {
        foreach (Letterbox letterBox in letterBoxes.Where(letterBox => letterBox != exceptLetterbox)) letterBox.DeselectBox();
    }

    public void SetRowInteractable(bool interactable)
    {
        foreach (Letterbox letterBox in letterBoxes)
            letterBox.SetInteractable(interactable);
        
        if (interactable)
        {
            letterBoxes[0].SelectBox();
            currentLetterBox = letterBoxes[0];
        }
    }

    public void SelectNext()
    {
        int index = letterBoxes.FindIndex(box => box == currentLetterBox);
        // int indexForLoop = index;
        // while(indexForLoop < letterBoxes.Count - 1)
        // {
        //     if (string.IsNullOrEmpty(letterBoxes[indexForLoop + 1].Letter))
        //     {
        //         letterBoxes[indexForLoop + 1].SelectBox();
        //         return;
        //     }
        //     
        //     indexForLoop++;
        // }
        
        //If still none is selected
        if(index < letterBoxes.Count - 1)
            letterBoxes[index + 1].SelectBox();
    }
    
    public void SelectPrevious()
    {
        int index = letterBoxes.FindIndex(box => box == currentLetterBox);
        // int indexForLoop = index;
        // while(indexForLoop > 0)
        // {
        //     if (string.IsNullOrEmpty(letterBoxes[indexForLoop - 1].Letter))
        //     {
        //         letterBoxes[indexForLoop - 1].SelectBox();
        //         return;
        //     }
        //     
        //     indexForLoop--;
        // }
        
        //If still none is selected
        if(index > 0)
            letterBoxes[index - 1].SelectBox();
    }

    public Letterbox GetCurrentLetterbox()
    {
        return currentLetterBox;
    }

    public void SetLetterStatuses(List<LetterStatus> statuses, bool playAnim = false)
    {
        for (int i = 0; i < letterBoxes.Count; i++)
            letterBoxes[i].SetLetterStatus(statuses[i], playAnim, i * 0.4f);
    }

    public List<LetterStatus> GetLetterStatuses()
    {
        return letterBoxes.Select(t => t.GetLetterStatus()).ToList();
    }

    public string GetSpelledWord()
    {
        return letterBoxes.Aggregate("", (current, letterBox) => current + letterBox.Letter);
    }

    private void Subscribe()
    {
        foreach (Letterbox letterBox in letterBoxes)
        {
            letterBox.SelectedRX.Subscribe(x =>
            {
                DeselectAllBut(x);
                currentLetterBox = x;
            }).AddTo(disposables);

            letterBox.LetterUpdatedRX.Subscribe(letter =>
            {
                if (string.IsNullOrEmpty(letter))
                    IsRowFilledRX.Value = false;
                else
                    IsRowFilledRX.Value = letterBoxes.All(letterbox => !string.IsNullOrEmpty(letterbox.Letter));

            }).AddTo(disposables);
        }
    }
}
