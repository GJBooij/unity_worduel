using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using worduel.models;

public class LetterboxGrid : MonoBehaviour
{
    public IReactiveProperty<bool> CanSubmitRX = new ReactiveProperty<bool>();
    // public static LetterboxGrid Instance;

    private List<LetterboxRow> rows;
    private LetterboxRow currentRow;
    private IDisposable rowFilledDisposable;

    private void Awake()
    {
        // if (Instance == null)
            // Instance = this;
        // else
            // Destroy(gameObject);
        
        rows = new List<LetterboxRow>();
        foreach (Transform child in transform)
            rows.Add(child.GetComponent<LetterboxRow>());
    }

    public void Init(List<string> savedSpelledWords = null, List<List<LetterStatus>> savedStatuses = null)
    {
        //Figure out the current row
        if(savedSpelledWords == null) currentRow = rows[0];
        else
        {
            for (var i = 0; i < savedSpelledWords.Count; i++)
            {
                if (string.IsNullOrEmpty(savedSpelledWords[i]))
                {
                    if (i == 0)
                        currentRow = rows[0];
                    else
                        currentRow = rows[i-1];
                    break;
                }
            }
        }

        if (currentRow == null)
            currentRow = rows.Last();
        
        for (var i = 0; i < rows.Count; i++)
        {
            rows[i].Init(savedSpelledWords?[i], savedStatuses?[i]);
            rows[i].SetRowInteractable(rows[i] == currentRow);
        }
        
        CanSubmitRX.Value = false;
        rowFilledDisposable?.Dispose();
        rowFilledDisposable = currentRow.IsRowFilledRX.Subscribe(x =>
        {
            CanSubmitRX.Value = x;
        });
    }

    public void SetLetter(string letter)
    {
        if (!string.IsNullOrEmpty(letter))
        {
            currentRow.GetCurrentLetterbox().SetLetter(letter);
            currentRow.SelectNext();
        }
        else
        {
            bool emptyPrev = string.IsNullOrEmpty(currentRow.GetCurrentLetterbox().Letter);
            currentRow.GetCurrentLetterbox().SetLetter(letter);
            
            if (emptyPrev)
            {
                currentRow.SelectPrevious();
                currentRow.GetCurrentLetterbox().SetLetter(letter);
            }
        }
    }

    public bool GoToNextRow()
    {
        int index = rows.IndexOf(currentRow);
        if (index >= rows.Count - 1)
            return false;
        
        currentRow.SetRowInteractable(false);
        currentRow = rows[index + 1];
        currentRow.SetRowInteractable(true);
        
        CanSubmitRX.Value = false;
        rowFilledDisposable?.Dispose();
        rowFilledDisposable = currentRow.IsRowFilledRX.Subscribe(x =>
        {
            CanSubmitRX.Value = x;
        });

        return true;
    }
    
    public LetterboxRow GetCurrentRow()
    {
        return currentRow; 
    }

    public int GetRowIndex()
    {
        return rows.IndexOf(currentRow);
    }

    public List<string> GetAllRowsSpelledWords()
    {
        return rows.Select(row => row.GetSpelledWord()).ToList();
    }

    public List<List<LetterStatus>> GetAllRowsStatuses()
    {
        return rows.Select(row => row.GetLetterStatuses()).ToList();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
            currentRow.SelectNext();
    }
}
