using System;
using Pixelplacement;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.Services;

public class Key : MonoBehaviour
{
    //Inspector variables
    [SerializeField] private Keyboard keyboard;
    [SerializeField] private string letter;
    [SerializeField] private Button keyButton;
    [SerializeField] private TMP_Text letterText;
    [SerializeField] private Image background;

    private void Awake()
    {
        if(!string.IsNullOrEmpty(letter)) 
            letterText.text = letter.Substring(0, 1).ToUpper();
        
        background.color = Constants.Colors.NormalKeyColor;
    }

    private void Start()
    {
        keyButton.OnClickAsObservable().Subscribe(x => { keyboard.LetterSubRX.OnNext(letter); }).AddTo(this);

        keyboard.LetterStatusesUpdatedRX.Subscribe(dict =>
        {
            if(dict.ContainsKey(letter))
                SetLetterStatus(dict[letter]);
            else
                SetLetterStatus(LetterStatus.None);

        }).AddTo(this);
    }

    public void SetLetterStatus(LetterStatus letterStatus)
    {
        switch (letterStatus)
        {
            case LetterStatus.None:
                background.color = Constants.Colors.NormalKeyColor;
                break;
            case LetterStatus.Wrong:
                background.color = Constants.Colors.WrongColor;
                break;
            case LetterStatus.Almost:
                background.color = Constants.Colors.AlmostColor;
                break;
            case LetterStatus.Good:
                background.color = Constants.Colors.CorrectColor;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(letterStatus), letterStatus, null);
        }
    }
}
