using System;
using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public enum LetterStatus
{
    None,
    Wrong,
    Almost,
    Good
}

public class Letterbox : MonoBehaviour
{
    public ISubject<Letterbox> SelectedRX = new Subject<Letterbox>();
    public ISubject<string> LetterUpdatedRX = new Subject<string>();
    
    [HideInInspector] public string Letter;
    
    [SerializeField] private Button boxButton;
    [SerializeField] private Image border;
    [SerializeField] private Image background;
    [SerializeField] private TMP_Text placeholder;
    [SerializeField] private TMP_Text letterText;
    
    [Header("Colors")]
    [SerializeField] private Color borderColorSelected;
    [SerializeField] private Color borderColorUnselected;

    private LetterStatus letterStatus;
    private bool interactable = true;

    public void Init(string savedLetter = "", LetterStatus savedStatus = LetterStatus.None)
    {
        placeholder.gameObject.SetActive(string.IsNullOrEmpty(savedLetter));
        letterText.text = savedLetter;
        Letter = savedLetter;

        SetLetterStatus(savedStatus, false);
        
        boxButton.OnClickAsObservable().Subscribe(x =>
        {
            SelectBox();
            if (interactable)
                Animate();
        }).AddTo(this);
    }

    public void SelectBox()
    {
        if (!interactable)
        {
            DeselectBox();
            return;
        }
        
        SelectedRX.OnNext(this);
        border.color = borderColorSelected;
    }

    public void DeselectBox()
    {
        border.color = ColorByStatus();
    }

    public void SetLetter(string letter)
    {
        placeholder.gameObject.SetActive(string.IsNullOrEmpty(letter));
        letterText.text = letter;
        Letter = letter;
        LetterUpdatedRX.OnNext(letter);

        if(!string.IsNullOrEmpty(letter))
        {
            Animate();
        }
    }

    public void SetInteractable(bool interactable)
    {
        this.interactable = interactable;
        boxButton.interactable = interactable;
        background.color = ColorByStatus();
        
        if(!interactable)
            DeselectBox();
    }

    public void SetLetterStatus(LetterStatus status, bool playAnim = false, float animDelay = 0)
    {
        letterStatus = status;

        if (!playAnim)
        {
            background.color = ColorByStatus();
            border.color = ColorByStatus();
        }
        else 
        {
            Tween.Rotate(transform, new Vector3(90, 0, 0), Space.Self, 0.2f, animDelay, Tween.EaseIn, Tween.LoopType.None, null,
                () =>
                {
                    background.color = ColorByStatus();
                    border.color = ColorByStatus();
                    Tween.Rotate(transform, new Vector3(-90, 0, 0), Space.Self, 0.2f, 0, Tween.EaseIn, Tween.LoopType.None, null,
                        () => { transform.eulerAngles = Vector3.zero; });
                });
        }
    }

    public LetterStatus GetLetterStatus()
    {
        return letterStatus;
    }

    private Color ColorByStatus()
    {
        switch (letterStatus)
        {
            case LetterStatus.None:
                return boxButton.interactable ? Constants.Colors.NormalColor : Constants.Colors.DisabledColor;
            case LetterStatus.Wrong:
                return Constants.Colors.WrongColor;
            case LetterStatus.Almost:
                return Constants.Colors.AlmostColor;
            case LetterStatus.Good:
                return Constants.Colors.CorrectColor;
            default:
                return Constants.Colors.NormalColor;
        }
    }

    private void Animate()
    {
        Tween.LocalScale(transform, Vector3.one + new Vector3(0.1f, 0.1f, 0.1f), 0.1f, 0, Tween.EaseBounce, Tween.LoopType.None, null,
            () =>
            {
                Tween.LocalScale(transform, Vector3.one, 0.1f, 0);
            });
    }
}
