using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class RemoveKey : MonoBehaviour
{
    [SerializeField] private Keyboard keyboard;
    [SerializeField] private Button keyButton;
    [SerializeField] private Image background;

    private void Start()
    {
        background.color = Constants.Colors.NormalKeyColor;
        keyButton.OnClickAsObservable().Subscribe(x => { keyboard.RemoveSubRX.OnNext(true); }).AddTo(this);
    }
}
