using System;
using Adic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.Services;

namespace Worduel.Scripts.UI
{
    public class TopBar : MonoBehaviour
    {
        //Inspector variables
        [SerializeField] private TMP_Text title;
        [SerializeField] private TMP_Text subtitle;
        [SerializeField] private Button backButton;
        
        //Injections
        [Inject] protected UIService uiService;
        
        //Private variables
        private Canvas canvas;

        private void Awake()
        {
            title.fontStyle = FontStyles.Normal;
            subtitle.text = "";
            backButton.gameObject.SetActive(false);

            canvas = GetComponent<Canvas>();
#if !UNITY_EDITOR
            CanvasScaler canvasScaler = GetComponent<CanvasScaler>();

            if (canvasScaler != null)
            {
                if (Utils.DeviceIsTablet())
                {
                    canvasScaler.referenceResolution = new Vector2(1536, 2048);
                }
                else
                {
                    canvasScaler.referenceResolution = new Vector2(1080, 1920);
                }
            }
#endif
        }

        private void Start()
        {
            this.Inject();
        }
        
        [Inject]
        protected void PostConstruct()
        {
            uiService.CurrentViewRX.Subscribe(viewType =>
            {
                switch (viewType)
                {
                    case ViewType.Home:
                        title.text = "WOORDUEL";
                        title.fontStyle = FontStyles.Normal;
                        subtitle.text = "";
                        backButton.gameObject.SetActive(false);
                        break;
                    case ViewType.WordOfTheDay5:
                        title.text = "5 LETTER";
                        title.fontStyle = FontStyles.Underline;
                        subtitle.text = "WOORD VAN DE DAG";
                        backButton.gameObject.SetActive(true);
                        break;
                    case ViewType.WordOfTheDay6:
                        title.text = "6 LETTER";
                        title.fontStyle = FontStyles.Underline;
                        subtitle.text = "WOORD VAN DE DAG";
                        backButton.gameObject.SetActive(true);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(viewType), viewType, null);
                }

            }).AddTo(this);

            backButton.OnClickAsObservable().Subscribe(x =>
            {
                uiService.GoToView(ViewType.Home);
            }).AddTo(this);
        }

        public void Show()
        {
            canvas.enabled = true;
        }

        public void Hide()
        {
            canvas.enabled = false;
        }
    }
}
