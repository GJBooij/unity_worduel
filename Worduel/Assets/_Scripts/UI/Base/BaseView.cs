﻿using Adic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.Services;

namespace Worduel.Scripts.UI
{
    public enum ViewType
    {
        Home,
        WordOfTheDay5,
        WordOfTheDay6
    }
    
    public class BaseView : MonoBehaviour
    {
        //Public variables
        public IReactiveProperty<UIState> StateRX { get; } = new ReactiveProperty<UIState>();
        public virtual ViewType Id { get; }

        //Injections
        [Inject] protected UIService uiService;

        //Private/Protected variables
        protected RectTransform wrapperRT;
        protected Canvas canvas;
        protected GraphicRaycaster rayCaster;

        private void Awake()
        {
            wrapperRT = transform.Find("Wrapper").GetComponent<RectTransform>();
            canvas = transform.GetComponent<Canvas>();
            rayCaster = transform.GetComponent<GraphicRaycaster>();

#if !UNITY_EDITOR
            CanvasScaler canvasScaler = GetComponent<CanvasScaler>();

            if (canvasScaler != null)
            {
                if (Utils.DeviceIsTablet())
                {
                    canvasScaler.referenceResolution = new Vector2(1536, 2048);
                }
                else
                {
                    canvasScaler.referenceResolution = new Vector2(1080, 1920);
                }
            }
#endif

            Hide();
        }

        private void Start()
        {
            this.Inject();
        }

        [Inject]
        protected void PostConstruct()
        {
            Init();
            Subscribe();
        }

        protected virtual void Init()
        {
        }

        protected virtual void Subscribe()
        {
        }

        public virtual void Show(ViewType previousView, bool shouldRefresh, params object[] viewParams)
        {
            if (StateRX.Value == UIState.Active)
                return;

            canvas.enabled = true;
            rayCaster.enabled = true;

            StateRX.Value = UIState.Active;
        }

        public virtual void Hide()
        {
            if (StateRX.Value == UIState.Inactive)
                return;

            canvas.enabled = false;
            rayCaster.enabled = false;
            
            StateRX.Value = UIState.Inactive;
        }

        public virtual void OnCloseButton() {}
    }
}
