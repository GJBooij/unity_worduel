﻿using Adic;
using UniRx;
using UnityEngine;
using UnityEngine.Scripting;
using UnityEngine.UI;
using Worduel.Scripts.Services;

namespace Worduel.Scripts.UI
{
    public enum ModalType
    {
        None,
        HowToPlay,
        PlayerStats,
        NoInternet,
        ForceUpdate,
        AppDisabled
    }
    
    public class BaseModal : MonoBehaviour
    {
        //Public variables
        public IReactiveProperty<UIState> StateRX = new ReactiveProperty<UIState>();
        public virtual ModalType Id { get; }

        //Injections
        [Inject] protected UIService uiService;

        //Private/Protected variables
        protected RectTransform wrapperRT;
        protected Canvas canvas;
        protected GraphicRaycaster rayCaster;

        private void Awake()
        {
            wrapperRT = transform.Find("Wrapper").GetComponent<RectTransform>();
            canvas = transform.GetComponent<Canvas>();
            rayCaster = transform.GetComponent<GraphicRaycaster>();
            
#if !UNITY_EDITOR
            CanvasScaler canvasScaler = GetComponent<CanvasScaler>();

            if (canvasScaler != null)
            {
                if (Utils.DeviceIsTablet())
                {
                    canvasScaler.referenceResolution = new Vector2(1536, 2048);
                }
                else
                {
                    canvasScaler.referenceResolution = new Vector2(1080, 1920);
                }
            }
#endif
        }

        public virtual void Start()
        {
            this.Inject();
        }

        [Inject, Preserve]
        protected void PostConstruct()
        {
            Init();
            Subscribe();
        }

        protected virtual void Init()
        {
        }

        protected virtual void Subscribe()
        {
        }

        public virtual void Show(params object[] modalParams)
        {
            if (StateRX.Value == UIState.Active)
                return;

            canvas.enabled = true;
            rayCaster.enabled = true;

            StateRX.Value = UIState.Active;
        }

        public virtual void Hide()
        {
            if (StateRX.Value == UIState.Inactive)
                return;

            canvas.enabled = false;
            rayCaster.enabled = false;

            StateRX.Value = UIState.Inactive;
        }
    }
}
