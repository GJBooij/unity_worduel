using System;
using System.Collections.Generic;
using EasyMobile;
using Pixelplacement;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using worduel.models;
using Worduel.Scripts.Services;
using Worduel.Scripts.UI;

public class PlayerStatsModal : BaseModal
{
    public override ModalType Id { get; } = ModalType.PlayerStats;

    //Inspector variables
    [Header("Game Services")]
    [SerializeField] private GameService gameServiceWOTD5;
    [SerializeField] private GameService gameServiceWOTD6;
    
    [Header("UI")]
    [SerializeField] private Button backgroundCloseButton;
    [SerializeField] private Button closeButton;
    [SerializeField] private Button shareButton;
    [SerializeField] private Button word6LetterButton;
    [SerializeField] private RectTransform container;
    [SerializeField] private TMP_Text gamesPlayedText;
    [SerializeField] private TMP_Text successRateText;
    [SerializeField] private TMP_Text currentStreakText;
    [SerializeField] private TMP_Text highestStreakText;
    [SerializeField] private TMP_Text nextGameTimerText;
    [SerializeField] private TMP_Text shareButtonText;
    
    [Header("Guesses")]
    [SerializeField] private List<RectTransform> guessBarImages;
    [SerializeField] private List<TMP_Text> guessTexts;

    //Private variables
    private List<List<LetterStatus>> rowsAndStatusesToShare;
    private DateTime midnight;
    private IDisposable secondsTimer;
    private GameType gameType;
    
    protected override void Init()
    {
        base.Init();
    }

    protected override void Subscribe()
    {
        base.Subscribe();
        
        Observable.Merge(backgroundCloseButton.OnClickAsObservable(),
            closeButton.OnClickAsObservable())
                .Subscribe(x =>
                {
                    uiService.HideModal();
                }).AddTo(this);

        shareButton.OnClickAsObservable().Subscribe(x =>
        {
            Sharing.ShareText(GetTextToShare());
        }).AddTo(this);

        word6LetterButton.OnClickAsObservable().Subscribe(x =>
        {
            uiService.HideModal();
            uiService.GoToView(ViewType.WordOfTheDay6);
        }).AddTo(this);
    }

    public override void Show(params object[] modalParams)
    {
        container.anchoredPosition = new Vector2(-1080, 0);

        //Get the global save data from the GameService.
        WOTDGlobalSaveData globalSaveData = modalParams[0] as WOTDGlobalSaveData;
        rowsAndStatusesToShare = modalParams[1] as List<List<LetterStatus>>;
        gameType = (GameType) modalParams[2];
        
        //First get the simple global statistics
        gamesPlayedText.text = globalSaveData.FinishedGames.Keys.Count.ToString();
        currentStreakText.text = globalSaveData.CurrentStreak.ToString();
        highestStreakText.text = globalSaveData.HighestStreak.ToString();
        
        if (globalSaveData.GamesWon.Count > 0 && globalSaveData.FinishedGames.Keys.Count > 0)
            successRateText.text = Mathf.CeilToInt(((float) globalSaveData.GamesWon.Count / globalSaveData.FinishedGames.Keys.Count) * 100).ToString();
        else
            successRateText.text = "0";

        //Then check the won games and the amount of guesses per game.
        Dictionary<int, List<int>> wonGamesByGuesses = new Dictionary<int, List<int>>();
        foreach (int gameId in globalSaveData.GamesWon)
        {
            if(!globalSaveData.FinishedGames.ContainsKey(gameId))
                continue;
            
            if(wonGamesByGuesses.ContainsKey(globalSaveData.FinishedGames[gameId].Guesses))
                wonGamesByGuesses[globalSaveData.FinishedGames[gameId].Guesses].Add(gameId);
            else
                wonGamesByGuesses.Add(globalSaveData.FinishedGames[gameId].Guesses, new List<int>(){gameId});
        }

        //Calculate the length and the color of the guess bar
        for (int i = 1; i <= 6; i++)
        {
            Color barColor = Constants.Colors.DisabledColor;
            
            if (wonGamesByGuesses.ContainsKey(i))
            {
                guessTexts[i - 1].text = wonGamesByGuesses[i].Count.ToString();
                
                float percentage = 0;
                if(wonGamesByGuesses[i].Count > 0 && globalSaveData.GamesWon.Count > 0)
                    percentage = (float)wonGamesByGuesses[i].Count / globalSaveData.GamesWon.Count;

                percentage = Mathf.Clamp(percentage, 0f, 1f);
                guessBarImages[i- 1].sizeDelta = new Vector2(Mathf.Max(40, 740 * percentage),guessBarImages[i- 1].sizeDelta.y);

                if (wonGamesByGuesses[i].Contains(ConfigDataService.Instance.WordOfTheDay.GameId))
                {
                    barColor = Constants.Colors.CorrectColor;
                }
            }
            else
            {
                guessTexts[i - 1].text = "0";
                guessBarImages[i- 1].sizeDelta = new Vector2(40,guessBarImages[i- 1].sizeDelta.y);
            }

            guessBarImages[i - 1].GetComponent<Image>().color = barColor;
        }
        
        //Check if we should show the timer or the play 6-letter word button
        bool showTimer = true;
        if (gameType == GameType.WOTD_5)
        {
            if (gameServiceWOTD5.GameStateRX.Value == GameState.Playing || gameServiceWOTD5.GameStateRX.Value == GameState.Checking)
                showTimer = true;
            else if (gameServiceWOTD6.GameStateRX.Value == GameState.Playing || gameServiceWOTD6.GameStateRX.Value == GameState.Checking)
                showTimer = false;
        }
        
        nextGameTimerText.gameObject.SetActive(showTimer);
        word6LetterButton.gameObject.SetActive(!showTimer);
        if (showTimer)
        {
            //Get midnight datetime
            midnight = DateTime.Today.AddDays(1);
            nextGameTimerText.text = midnight.Subtract(DateTime.Now).ToString(@"hh\:mm\:ss");
        
            //Start a timer for every second
            secondsTimer?.Dispose();
            secondsTimer = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(x =>
            {
                midnight = DateTime.Today.AddDays(1);
                nextGameTimerText.text = midnight.Subtract(DateTime.Now).ToString(@"hh\:mm\:ss");
            });
        }

        //Check if the user is allowed to share
        shareButton.interactable = globalSaveData.FinishedGames.ContainsKey(ConfigDataService.Instance.WordOfTheDay.GameId);
        shareButtonText.text = shareButton.interactable ? "Delen" : "Delen kan na\nhet spelen";
        
        base.Show(modalParams);

        Tween.Stop(GetInstanceID());
        Tween.AnchoredPosition(container, Vector2.zero, 0.25f, 0.2f);
    }

    public override void Hide()
    {
        Tween.AnchoredPosition(container, new Vector2(1080, 0), 0.3f, 0, Tween.EaseLinear, Tween.LoopType.None, null, () =>
        {
            base.Hide();
            secondsTimer?.Dispose();
        });
    }

    /// <summary>
    /// Function to get the text that is used for sharing
    /// </summary>
    private string GetTextToShare()
    {
        string shareText = "Woorduel - " + ConfigDataService.Instance.WordOfTheDay.GameId;
        shareText += "\n" + (gameType == GameType.WOTD_5 ? "5-Letter woord" : "6-Letter woord");
        shareText += "\n";

        int guesses = 0;
        string rowsText = "";
        
        foreach (List<LetterStatus> statuses in rowsAndStatusesToShare)
        {
            if (statuses[0] == LetterStatus.None)
                break;

            foreach (LetterStatus status in statuses)
            {
                switch (status)
                {
                    case LetterStatus.None:
                        break;
                    case LetterStatus.Wrong:
                        rowsText += "⬛";
                        break;
                    case LetterStatus.Almost:
                        rowsText += "🟨";
                        break;
                    case LetterStatus.Good:
                        rowsText += "🟩";
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            rowsText += "\n";
            guesses++;
        }

        shareText += "\n" + guesses + "/6";
        shareText += "\n" + rowsText;
        
        return shareText;
    }
}
