using Adic;
using Pixelplacement;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.Services;
using Worduel.Scripts.UI;

public class AppDisabledModal : BaseModal
{
    public override ModalType Id { get; } = ModalType.AppDisabled;

    [SerializeField] private RectTransform container;
    [SerializeField] private Button closeButton;

    protected override void Init()
    {
        base.Init();
    }

    protected override void Subscribe()
    {
        base.Subscribe();

        closeButton.OnClickAsObservable()
            .Subscribe(x => Application.Quit()).AddTo(this);
    }

    public override void Show(params object[] modalParams)
    {
        base.Show(modalParams);
    }

    public override void Hide()
    {
        base.Hide();
    }
}
