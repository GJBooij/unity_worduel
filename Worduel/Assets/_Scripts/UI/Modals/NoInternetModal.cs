using Adic;
using Pixelplacement;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.Services;
using Worduel.Scripts.UI;

public class NoInternetModal : BaseModal
{
    public override ModalType Id { get; } = ModalType.NoInternet;

    [SerializeField] private RectTransform container;
    [SerializeField] private Button tryAgainButton;
    [SerializeField] private Button closeButton;

    protected override void Init()
    {
        base.Init();
    }

    protected override void Subscribe()
    {
        base.Subscribe();
        tryAgainButton.OnClickAsObservable()
            .Subscribe(x =>
            {
                uiService.HideModal();
                
                Observable.NextFrame().Subscribe(y =>
                {
                    ConfigDataService.Instance.CheckInternetAndGetConfigs();
                });
            }).AddTo(this);

        closeButton.OnClickAsObservable()
            .Subscribe(x => Application.Quit()).AddTo(this);
    }

    public override void Show(params object[] modalParams)
    {
        base.Show(modalParams);
    }

    public override void Hide()
    {
        base.Hide();
    }
}
