using Adic;
using Pixelplacement;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.Services;
using Worduel.Scripts.UI;

public class ForceUpdateModal : BaseModal
{
    public override ModalType Id { get; } = ModalType.ForceUpdate;

    [SerializeField] private RectTransform container;
    [SerializeField] private TMP_Text forceUpdateText;
    [SerializeField] private Button storeButton;
    [SerializeField] private Button closeButton;

    protected override void Init()
    {
        base.Init();
    }

    protected override void Subscribe()
    {
        base.Subscribe();
        storeButton.OnClickAsObservable()
            .Subscribe(x =>
            {
#if UNITY_ANDROID
                Application.OpenURL(ConfigDataService.Instance.AppConfig.StoreLinkAndroid);
#elif UNITY_IOS
                Application.OpenURL(ConfigDataService.Instance.AppConfig.StoreLinkIOS);
#endif
                Application.Quit();
            }).AddTo(this);

        closeButton.OnClickAsObservable()
            .Subscribe(x => Application.Quit()).AddTo(this);
    }

    public override void Show(params object[] modalParams)
    {
        if(!string.IsNullOrEmpty(ConfigDataService.Instance.AppConfig.ForceUpdateText))
            forceUpdateText.text = ConfigDataService.Instance.AppConfig.ForceUpdateText;
        
        base.Show(modalParams);
    }

    public override void Hide()
    {
        base.Hide();
    }
}
