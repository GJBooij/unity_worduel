using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.UI;

public class HowToPlayModal : BaseModal
{
    public override ModalType Id { get; } = ModalType.HowToPlay;

    [SerializeField] private Button backgroundCloseButton;
    [SerializeField] private Button closeButton;
    [SerializeField] private RectTransform container;
    [SerializeField] private Image goodLetter;
    [SerializeField] private Image almostLetter;
    [SerializeField] private Image wrongLetter;

    protected override void Init()
    {
        base.Init();
        goodLetter.color = Constants.Colors.CorrectColor;
        almostLetter.color = Constants.Colors.AlmostColor;
        wrongLetter.color = Constants.Colors.WrongColor;
    }

    protected override void Subscribe()
    {
        base.Subscribe();
        Observable.Merge(backgroundCloseButton.OnClickAsObservable(),
            closeButton.OnClickAsObservable())
                .Subscribe(x =>
                {
                    uiService.HideModal();
                }).AddTo(this);
    }

    public override void Show(params object[] modalParams)
    {
        container.anchoredPosition = new Vector2(-1080, 0);
        base.Show(modalParams);

        Tween.Stop(GetInstanceID());
        Tween.AnchoredPosition(container, Vector2.zero, 0.25f, 0.2f);
    }

    public override void Hide()
    {
        CheckSaveOnboarding();
        
        Tween.AnchoredPosition(container, new Vector2(1080, 0), 0.3f, 0, Tween.EaseLinear, Tween.LoopType.None, null, () =>
        {
            base.Hide();
        });
    }

    private void CheckSaveOnboarding()
    {
        if (PlayerPrefs.HasKey(Constants.PlayerPrefsKeys.ONBOARDING_GAME) && PlayerPrefs.GetInt(Constants.PlayerPrefsKeys.ONBOARDING_GAME) == 1)
            return;
        
        PlayerPrefs.SetInt(Constants.PlayerPrefsKeys.ONBOARDING_GAME, 1);
        PlayerPrefs.Save();
    }
}
