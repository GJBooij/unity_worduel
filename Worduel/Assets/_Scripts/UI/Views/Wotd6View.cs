using System;
using Pixelplacement;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.Services;
using Worduel.Scripts.UI;

public class Wotd6View : BaseView
{
    public override ViewType Id { get; } = ViewType.WordOfTheDay6;

    //Inspector variables
    [SerializeField] private GameService gameService;
    [SerializeField] private Keyboard keyboard;
    [SerializeField] private LetterboxGrid lettersGrid;
    [SerializeField] private TMP_Text unkownWordText;
    [SerializeField] private TMP_Text gameOverText;
    [SerializeField] private Button howToPlayButton;
    [SerializeField] private Button statisticsButton;

    protected override void Init()
    {
        base.Init();
        unkownWordText.gameObject.SetActive(false);
        gameOverText.gameObject.SetActive(false);
    }

    protected override void Subscribe()
    {
        base.Subscribe();
        
        //Keyboard subs
        keyboard.LetterSubRX.Subscribe(letter =>
        {
            if(gameService.GameStateRX.Value == GameState.Playing)
                lettersGrid.SetLetter(letter);
        }).AddTo(this);

        keyboard.CheckWordSubRX.Subscribe(x =>
        {
            if (!x) return;
            gameService.CheckWord();
        });
        
        keyboard.RemoveSubRX.Subscribe(x =>
        {
            if (!x) return;
            if(gameService.GameStateRX.Value == GameState.Playing)
                lettersGrid.SetLetter("");
        });
        
        gameService.LetterStatusesUpdatedRX.Subscribe(x => { keyboard.LetterStatusesUpdatedRX.OnNext(x); }).AddTo(this);

        //Game service sub to unknown word
        gameService.UnknownWordRX.Subscribe(playerWord =>
        {
            unkownWordText.text = playerWord + " staat niet in ons woordenboek.";
            unkownWordText.alpha = 0;
            unkownWordText.gameObject.SetActive(true);
            
            Tween.Stop(GetInstanceID());
            Tween.Value(0f, 1.0f, f => { unkownWordText.alpha = f; }, 0.3f, 0, Tween.EaseIn, Tween.LoopType.None, null,
    () =>
                {
                    Observable.Timer(TimeSpan.FromSeconds(2)).Subscribe(x =>
                    {
                        Tween.Value(1.0f, 0f, f => { unkownWordText.alpha = f; }, 0.2f, 0, Tween.EaseIn, Tween.LoopType.None, null,
                            () =>
                            {
                                unkownWordText.gameObject.SetActive(false);
                            });
                    });
                });

        }).AddTo(this);

        //Game service sub to game state
        gameService.GameStateRX.Subscribe(state =>
        {
            if (state == GameState.Won || state == GameState.GameOver)
            {
                if (state == GameState.Won)
                    gameOverText.text = "Goed gedaan!\nJe hebt het woord van de dag geraden.";
                else
                    gameOverText.text = "Helaas, het woord van vandaag was: \n" + ConfigDataService.Instance.WordOfTheDay.WordNL6.ToUpper();
                
                gameOverText.gameObject.SetActive(true);
            }
            else
                gameOverText.gameObject.SetActive(false);
            
            //Update the keyboard.
            keyboard.CanSubmitRX.OnNext(state == GameState.Playing && lettersGrid.CanSubmitRX.Value);
        }).AddTo(this);
        
        //Subscribe to letterbox grid can submit Sub to update keyboard
        lettersGrid.CanSubmitRX.Subscribe(x =>
        {
            keyboard.CanSubmitRX.OnNext(x && gameService.GameStateRX.Value == GameState.Playing);
        }).AddTo(this);

        //Top bar buttons
        howToPlayButton.OnClickAsObservable().Subscribe(x => uiService.ShowModal(ModalType.HowToPlay)).AddTo(this);
        statisticsButton.OnClickAsObservable().Subscribe(x =>
        {
            uiService.ShowModal(ModalType.PlayerStats, gameService.GetGlobalSaveData(), lettersGrid.GetAllRowsStatuses(), gameService.GetGameType());
        }).AddTo(this);
    }

    public override void Show(ViewType previousView, bool shouldRefresh, params object[] viewParams)
    {
        base.Show(previousView, shouldRefresh, viewParams);
        
        if(ShouldShowOnboarding())
            uiService.ShowModal(ModalType.HowToPlay);
        else if(gameService != null && (gameService.GameStateRX.Value == GameState.Won || gameService.GameStateRX.Value == GameState.GameOver))
            uiService.ShowModal(ModalType.PlayerStats, gameService.GetGlobalSaveData(), lettersGrid.GetAllRowsStatuses(), gameService.GetGameType());
    }

    public override void Hide()
    {
        base.Hide();
    }

    private bool ShouldShowOnboarding()
    {
        if(PlayerPrefs.HasKey(Constants.PlayerPrefsKeys.ONBOARDING_GAME))
            if (PlayerPrefs.GetInt(Constants.PlayerPrefsKeys.ONBOARDING_GAME) == 1)
                return false;

        return true;
    }
}
