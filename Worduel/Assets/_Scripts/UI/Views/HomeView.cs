using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Worduel.Scripts.Services;
using Worduel.Scripts.UI;

public class HomeView : BaseView
{
    public override ViewType Id { get; } = ViewType.Home;

    //Inspector variables
    [Header("Game Services")]
    [SerializeField] private GameService gameService5;
    [SerializeField] private GameService gameService6;
    
    [Header("UI")]
    [SerializeField] private GameObject loader;
    [SerializeField] private GameObject container;
    [SerializeField] private GameObject seperator;
    [SerializeField] private TMP_Text nextGameTimerText;
    
    [Header("5-Letter word")]
    [SerializeField] private Button playWotd5Button;
    [SerializeField] private TMP_Text playWotd5ButtonText;
    
    [Header("6-Letter word")]
    [SerializeField] private Button playWotd6Button;
    [SerializeField] private TMP_Text playWotd6ButtonText;
    [SerializeField] private Image lockedIcon;
    [SerializeField] private TMP_Text lockedExplainText;

    //Private variables
    private bool minimalTimerReady, gameService5Ready, gameService6Ready;
    private DateTime midnight;
    private IDisposable secondsTimer;
    
    protected override void Init()
    {
        loader.SetActive(true);
        container.SetActive(false);
        
        base.Init();
        Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe(x =>
        {
            minimalTimerReady = true;
            CheckHideLoader();
        });
    }

    protected override void Subscribe()
    {
        base.Subscribe();
        
        if(ConfigDataService.Instance.ConfigServiceStatusRX.Value == ConfigServiceStatus.NoInternet)
            uiService.ShowModal(ModalType.NoInternet);
        else if(ConfigDataService.Instance.ConfigServiceStatusRX.Value == ConfigServiceStatus.ForceUpdate)
            uiService.ShowModal(ModalType.ForceUpdate);
        else if(ConfigDataService.Instance.ConfigServiceStatusRX.Value == ConfigServiceStatus.AppDisabled)
            uiService.ShowModal(ModalType.AppDisabled);
        else
        {
            ConfigDataService.Instance.ConfigServiceStatusRX.Subscribe(status =>
            {
                if(status == ConfigServiceStatus.NoInternet)
                    uiService.ShowModal(ModalType.NoInternet);
                else if(status == ConfigServiceStatus.ForceUpdate)
                    uiService.ShowModal(ModalType.ForceUpdate);
                else if(status == ConfigServiceStatus.AppDisabled)
                    uiService.ShowModal(ModalType.AppDisabled);
            }).AddTo(this);
        }

        GameService5Sub();
        GameService6Sub();

        //Subscribe to click on play 5-letter WOTD
        playWotd5Button.OnClickAsObservable().Subscribe(x =>
        {
            uiService.GoToView(ViewType.WordOfTheDay5);
        }).AddTo(this);
        
        //Subscribe to click on play 6-letter WOTD
        playWotd6Button.OnClickAsObservable().Subscribe(x =>
        {
            uiService.GoToView(ViewType.WordOfTheDay6);
        }).AddTo(this);
    }

    public override void Show(ViewType previousView, bool shouldRefresh, params object[] viewParams)
    {
        base.Show(previousView, shouldRefresh, viewParams);
    }

    public override void Hide()
    {
        base.Hide();
    }

    private void GameService5Sub()
    {
        if (gameService5.ServiceReadyRX.Value)
        {
            CheckGameState();
            gameService5Ready = true;
            CheckHideLoader();
        }
        else
            gameService5.ServiceReadyRX.Subscribe(ready =>
            {
                gameService5Ready = ready;
                CheckHideLoader();
                
                if (ready) CheckGameState();
            }).AddTo(this);

        gameService5.GameStateRX.Subscribe(state =>
        {
            playWotd5ButtonText.text = (state == GameState.Won || state == GameState.GameOver) ? "Bekijk resultaat" : "Spelen";
                
            //Check if the 6-letter can be unlocked.
            playWotd6Button.interactable = (state == GameState.Won || state == GameState.GameOver);
            lockedIcon.gameObject.SetActive(state == GameState.Playing || state == GameState.Checking);
            lockedExplainText.gameObject.SetActive(state == GameState.Playing || state == GameState.Checking);
            playWotd6ButtonText.rectTransform.anchoredPosition = new Vector2(lockedIcon.gameObject.activeSelf ? 35 : 0, playWotd6ButtonText.rectTransform.anchoredPosition.y);

        }).AddTo(this);
        
        //Internal function to check the current Game State.
        void CheckGameState()
        {
            GameState currentState = gameService5.GameStateRX.Value;
            playWotd5ButtonText.text = (currentState == GameState.Won || currentState == GameState.GameOver) ? "Bekijk resultaat" : "Spelen";
            
            //Check if the 6-letter should already be unlocked.
            playWotd6Button.interactable = (currentState == GameState.Won || currentState == GameState.GameOver);
            lockedIcon.gameObject.SetActive(currentState == GameState.Playing || currentState == GameState.Checking);
            lockedExplainText.gameObject.SetActive(currentState == GameState.Playing || currentState == GameState.Checking);
            playWotd6ButtonText.rectTransform.anchoredPosition = new Vector2(lockedIcon.gameObject.activeSelf ? 35 : 0, playWotd6ButtonText.rectTransform.anchoredPosition.y);
        }
    }

    private void GameService6Sub()
    {
        if (gameService6.ServiceReadyRX.Value)
        {
            CheckGameState();
            gameService6Ready = true;
            CheckHideLoader();
        }
        else
            gameService6.ServiceReadyRX.Subscribe(ready =>
            {
                gameService6Ready = ready;
                CheckHideLoader();
                
                if (ready) CheckGameState();
            }).AddTo(this);
        
        gameService6.GameStateRX.Subscribe(state =>
        {
            playWotd6ButtonText.text = (state == GameState.Won || state == GameState.GameOver) ? "Bekijk resultaat" : "Spelen";

            if (state == GameState.Won || state == GameState.GameOver)
                ShowTimer();
            else
                HideTimer();
                
        }).AddTo(this);
        
        //Internal function to check the current Game State.
        void CheckGameState()
        {
            GameState currentState = gameService6.GameStateRX.Value;

            playWotd6ButtonText.text = (currentState == GameState.Won || currentState == GameState.GameOver) ? "Bekijk resultaat" : "Spelen";
            if (currentState == GameState.Won || currentState == GameState.GameOver)
                ShowTimer();
            else
                HideTimer();
        }
    }
    
    private void CheckHideLoader()
    {
        if (!minimalTimerReady || !gameService5Ready || !gameService6Ready)
        {
            uiService.HideTopBar();
            loader.SetActive(true);
            container.SetActive(false);
            return;
        }
        
        uiService.ShowTopBar();
        loader.SetActive(false);
        container.SetActive(true);
    }

    private void ShowTimer()
    {
        seperator.SetActive(false);
        nextGameTimerText.gameObject.SetActive(true);
        
        //Get midnight datetime
        midnight = DateTime.Today.AddDays(1);
        nextGameTimerText.text = midnight.Subtract(DateTime.Now).ToString(@"hh\:mm\:ss");
        
        //Start a timer for every second
        secondsTimer?.Dispose();
        secondsTimer = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(x =>
        {
            midnight = DateTime.Today.AddDays(1);
            nextGameTimerText.text = midnight.Subtract(DateTime.Now).ToString(@"hh\:mm\:ss");
        });
    }

    private void HideTimer()
    {
        seperator.SetActive(true);
        nextGameTimerText.gameObject.SetActive(false);
        secondsTimer?.Dispose();
    }
}
