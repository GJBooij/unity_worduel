﻿using UniRx;
using Worduel.Scripts.Services;

namespace Worduel.Scripts.Interfaces.Services
{
    public interface ISceneService
    {
        IReactiveProperty<SceneName> CurrentSceneRX { get; }

        void GoToScene(SceneName scene);
    }
}
