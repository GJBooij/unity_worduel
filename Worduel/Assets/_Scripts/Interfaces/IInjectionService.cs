﻿using Adic.Container;

namespace Worduel.Scripts.Interfaces.Services
{
    public interface IInjectionService
    {
        IInjectionContainer Container { get; }
    }
}
