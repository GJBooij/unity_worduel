using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace worduel.models
{
    [DataContract]
    public class AppConfiguration
    {
        [DataMember(Name = "db_version_nl_5")] public int DBVersionNL5 { get; set; }
        [DataMember(Name = "db_version_nl_6")] public int DBVersionNL6 { get; set; }
        [DataMember(Name = "app_disabled")] public bool AppDisabled { get; set; }
        [DataMember(Name = "minimum_build_number_android")] public int MinimumBuildNumberAndroid { get; set; }
        [DataMember(Name = "minimum_build_number_ios")] public int MinimumBuildNumberIOS { get; set; }
        [DataMember(Name = "force_update_text")] public string ForceUpdateText { get; set; }
        [DataMember(Name = "store_link_android")] public string StoreLinkAndroid { get; set; }
        [DataMember(Name = "store_link_ios")] public string StoreLinkIOS { get; set; }
    }
    
    [DataContract]
    public class WordsDatabase
    {
        [DataMember(Name = "version")] public int Version { get; set; }
        [DataMember(Name = "language")] public string Language { get; set; }
        [DataMember(Name = "letters")] public int Letters { get; set; }
        [DataMember(Name = "words")] public List<string> Words { get; set; }
    }
    
    [DataContract]
    public class WordOfTheDay
    {
        [DataMember(Name = "gameId")] public int GameId { get; set; }
        [DataMember(Name = "word_nl_5")] public string WordNL5 { get; set; }
        [DataMember(Name = "word_nl_6")] public string WordNL6 { get; set; }
    }
    
    [DataContract]
    public class WOTDSaveData
    {
        [DataMember(Name = "gameId")] public int GameId { get; set; }
        [DataMember(Name = "words_per_row")] public List<string> RowSpelledWords { get; set; }
        [DataMember(Name = "statuses_per_row")] public List<List<LetterStatus>> RowLetterStatuses { get; set; }
        [DataMember(Name = "statuses_global")] public Dictionary<string, LetterStatus> GlobalLetterStatuses { get; set; }
    }
    
    [DataContract]
    public class WOTDFinishedGameData
    {
        [DataMember(Name = "gameId")] public int GameId { get; set; }
        [DataMember(Name = "guesses")] public int Guesses { get; set; }
        [DataMember(Name = "max_guesses")] public int MaxGuesses { get; set; }
        [DataMember(Name = "letters")] public int Letters { get; set; }
        [DataMember(Name = "word")] public string Word { get; set; }
    }
    
    [DataContract]
    public class WOTDGlobalSaveData
    {
        [DataMember(Name = "finished_games")] public Dictionary<int, WOTDFinishedGameData> FinishedGames { get; set; }
        [DataMember(Name = "games_won")] public List<int> GamesWon { get; set; }
        [DataMember(Name = "games_lost")] public List<int> GamesLost { get; set; }
        [DataMember(Name = "current_streak")] public int CurrentStreak { get; set; }
        [DataMember(Name = "highest_streak")] public int HighestStreak { get; set; }
    }
}

