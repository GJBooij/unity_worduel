using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Utils
{
    public static void Shuffle<T>(this IList<T> ts) {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i) {
            var r = Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
    
    public static bool DeviceIsTablet()
    {
        bool isTablet = false;

        float screenWidth = Screen.width / Screen.dpi;
        float screenHeight = Screen.height / Screen.dpi;
        float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));

#if UNITY_IOS && !UNITY_EDITOR
        if (SystemInfo.deviceModel.ToLower().Contains("ipad") || SystemInfo.deviceModel.ToLower().Contains("iphone"))
        {
            if (SystemInfo.deviceModel.ToLower().Contains("ipad"))
            {
                isTablet = true;
            }
        }else{
            isTablet = diagonalInches > 7.0f;
        }
#endif
#if UNITY_ANDROID || UNITY_EDITOR
        isTablet = diagonalInches > 7.0f;
#endif
        return isTablet;
    }

    public static T RandomEnumValue<T> ()
    {
        var v = Enum.GetValues (typeof (T));
        return (T) v.GetValue (Random.Range(0, v.Length - 1));
    }
    
    /// <param name="message">Message string to show in the toast.</param>
    public static void ShowAndroidToastMessage(string message)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
                toastObject.Call("show");
            }));
        }
#endif
    }
}
