﻿using System.Runtime.InteropServices;
using UnityEngine;

public static class iOSBuildVersionChecker {

// ===== General methods available for every platform =====

    public static int GetBuildVersion()
    {
#if UNITY_IOS && !UNITY_EDITOR
        return iOSGetBuildVersion();
#else
        return 0;
#endif
    }

    // ===== iOS implementations =====

#if UNITY_IOS && !UNITY_EDITOR
    [DllImport ("__Internal")] 
    private static extern int iOSGetBuildVersion();

    public static int GetiOSBuildNr(){
       return iOSGetBuildVersion();
    }
#endif

}
