#import "Foundation/Foundation.h"
#import <AVFoundation/AVFoundation.h>

@interface iOSBuildVersion : NSObject

#ifdef __cplusplus
extern "C" {
#endif
    
    int iOSGetBuildVersion();

#ifdef __cplusplus
}
#endif

@end
