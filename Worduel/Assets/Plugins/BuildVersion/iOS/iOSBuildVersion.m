#import "iOSBuildVersion.h"

@implementation iOSBuildVersion

int iOSGetBuildVersion()
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    int buildNr = [minorVersion intValue];
    
    return buildNr;
}

@end
