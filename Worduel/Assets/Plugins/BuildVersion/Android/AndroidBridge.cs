﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AndroidBridge
{
    public static string GetBuildVersion()
    {
        AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject bridge = new AndroidJavaObject("com.samhoud.bridge.Functions");

        object[] parameters = new object[1];
        parameters[0] = unityActivity;

        return bridge.Call<string>("GetBuildNumber", parameters);
    }
}