using System.Collections;
using System.Collections.Generic;
using System.Net;
using NetCheckerLib.Net;
using UnityEngine;

public static class NetworkStatus
{
    public delegate void _WiFiCheck(bool connectionWorking);
    public static event _WiFiCheck wifiCheckDone;

    private static int connectionTries = 0;

    public static void CheckWifiConnection()
    {
        NetChecker.OnCheckFinished += OnNetStatusChanged;
        NetChecker.OnCheckTimeout += OnNetStatusChanged;
        NetChecker.StartConnectionCheck();
    }

    private static void OnNetStatusChanged()
    {
        if (NetChecker.Status != NetStatus.Connected && NetChecker.Status != NetStatus.PendingCheck)
        {
            //fallback mechanism
            NetChecker.StopConnectionCheck();
            NetChecker.OnCheckFinished -= OnNetStatusChanged;
            NetChecker.OnCheckTimeout -= OnNetStatusChanged;

            if (connectionTries == 0)
            {
#if UNITY_ANDROID
                //Retry with Apple server
                NetChecker.UseMethod(NetChecker.GetAppleHotspotMethod());
#elif UNITY_IOS
                //Retry with Google server
                NetChecker.UseMethod(NetChecker.GetGoogle204Method());
#endif
                connectionTries++;
                CheckWifiConnection();
            }
            else if(connectionTries == 1)
            {
                //Retry with AH server
                NetCheckMethod ahCheckMethod = new NetCheckMethod("ah200", "https://www.ah.nl/", HttpStatusCode.OK);
                NetChecker.UseMethod(ahCheckMethod);
                connectionTries++;
                CheckWifiConnection();
            }
            else
            {
                connectionTries = 0;
                wifiCheckDone?.Invoke(false);
                wifiCheckDone = null;
                Debug.Log("Internet connection not available and connected to WiFi");
            }
        }
        else if(NetChecker.Status == NetStatus.Connected)
        {
            NetChecker.StopConnectionCheck();
            NetChecker.OnCheckFinished -= OnNetStatusChanged;
            NetChecker.OnCheckTimeout -= OnNetStatusChanged;
            connectionTries = 0;
            wifiCheckDone?.Invoke(true);
            wifiCheckDone = null;
            Debug.Log("Internet connection availabe and connected to WiFi");
        }
        else if (NetChecker.Status == NetStatus.WalledGarden)
        {
            NetChecker.StopConnectionCheck();
            NetChecker.OnCheckFinished -= OnNetStatusChanged;
            NetChecker.OnCheckTimeout -= OnNetStatusChanged;
            connectionTries = 0;
            wifiCheckDone?.Invoke(false);
            wifiCheckDone = null;
        }
        else if (NetChecker.Status == NetStatus.NoDNSConnection)
        {
            NetChecker.StopConnectionCheck();
            NetChecker.OnCheckFinished -= OnNetStatusChanged;
            NetChecker.OnCheckTimeout -= OnNetStatusChanged;
            connectionTries = 0;
            wifiCheckDone?.Invoke(false);
            wifiCheckDone = null;
        }
    }

}
